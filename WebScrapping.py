# -*- coding: utf-8 -*-
"""
Created on Mon Jun 22 12:52:15 2020
The below code basically used to collect data from an URL and to maintain data in an Excel File
This is an example of web-scrapping using Selenium
@author: soura
"""

import selenium
import openpyxl
from selenium import webdriver
from openpyxl import load_workbook

workbook = load_workbook('X:\\Data science\\USA_Accident_2018.xlsx')
sheet1 = workbook.active
webDriver =  webdriver.Chrome(executable_path='X:\Data science\chromedriver.exe')
webDriver.get('https://www.iihs.org/topics/fatality-statistics/detail/state-by-state#deaths-by-road-user')
count_state = 1
count_val = 1


for i in range(1,8):
    xpath = "//table[@is-sticky='true']//th[contains(text(),'Population, fatal motor vehicle crashes')]//parent::tr//following-sibling::tr//th[{num}]".format(num=str(i))
    title = webDriver.find_element_by_xpath(xpath)
    print(title.text)
    sheet1.cell(row=1,column=i).value = title.text


for i in range(1,53):
    xpath = "//table[@is-sticky='true']//th[contains(text(),'Population, fatal motor vehicle crashes')]//parent::tr//parent::thead//following-sibling::tbody//tr[{num}]//th[1]".format(num=str(i))
    title = webDriver.find_element_by_xpath(xpath)
    print(title.text)
    count_state += 1 
    sheet1.cell(row=count_state,column=1).value = title.text

for i in range(1,53):
    count_val += 1
    for j in range(1,7):
        xpath = "//table[@is-sticky='true']//th[contains(text(),'Population, fatal motor vehicle crashes')]//parent::tr//parent::thead//following-sibling::tbody//tr[{row}]//td[{col}]".format(row=str(i),col=str(j))
        title = webDriver.find_element_by_xpath(xpath)
        print(title.text)
        sheet1.cell(row=count_val,column=j+1).value = title.text
    


workbook.save('X:\\Data science\\USA_Accident_2018.xlsx')

